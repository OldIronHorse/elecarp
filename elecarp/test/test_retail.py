from unittest import TestCase
from unittest.mock import patch
from elecarp.retail import Receipt, calculate_receipt, order_value, \
    apply_tax, apply_discount, parse_args

class TestCalculateReceipt(TestCase):
    @patch('elecarp.retail.apply_tax')
    @patch('elecarp.retail.apply_discount')
    @patch('elecarp.retail.order_value')
    def test_valid_args(self, order_value, apply_discount, apply_tax):
        order_value.return_value = Receipt(
            quantity=123, price=45.67, order_value=5617.41)
        apply_discount.return_value = Receipt(
            quantity=123, price=45.67, order_value=5617.41, 
            discount_rate_pc=5, discount_amount=280.87,
            discounted_order_value=5336.54)
        apply_tax.return_value = Receipt(
            quantity=123, price=45.67, order_value=5617.41, 
            discount_rate_pc=5, discount_amount=280.87,
            discounted_order_value=5336.54,
            tax_rate_pc=6.85, tax_amount=365.55, total=5702.09)
        receipt = calculate_receipt(['123', '45.67', 'UT'])
        order_value.assert_called_once_with(Receipt(quantity=123, price=45.67))
        apply_discount.assert_called_once_with(Receipt(
            quantity=123, price=45.67, order_value=5617.41))
        apply_tax.assert_called_once_with(Receipt(
            quantity=123, price=45.67, order_value=5617.41, 
            discount_rate_pc=5, discount_amount=280.87,
            discounted_order_value=5336.54), 6.85)
        self.assertEqual(Receipt(
            quantity=123, price=45.67, order_value=5617.41, 
            discount_rate_pc=5, discount_amount=280.87,
            discounted_order_value=5336.54,
            tax_rate_pc=6.85, tax_amount=365.55, total=5702.09),
            receipt)

    def test_no_such_state(self):
        with self.assertRaises(KeyError):
            calculate_receipt(['123', '45.67', 'No such state!'])


class TestParseArgs(TestCase):
    def test_valid(self):
        self.assertEqual(
            (123, 45.67, 'UT'), parse_args(['123', '45.67', 'UT']))

    def test_invalid_quantity_fractional(self):
        with self.assertRaises(ValueError):
            parse_args(['123.5', '45.67', 'UT'])

    def test_invalid_quantity_non_numeric(self):
        with self.assertRaises(ValueError):
            parse_args(['ten', '45.67', 'UT'])

    def test_invalid_price_non_numeric(self):
        with self.assertRaises(ValueError):
            parse_args(['123', 'ten', 'UT'])


class TestOrderValue(TestCase):
    def test_order_value(self):
        self.assertEqual(Receipt(quantity=123, price=2.50, order_value=307.5),
            order_value(Receipt(123, 2.5)))


class TestApplyTax(TestCase):
    def test_apply_tax(self):
        self.assertEqual(
            Receipt(
                discounted_order_value=307.5,
                tax_rate_pc=6.85,
                tax_amount=21.06, 
                total=328.56),
            apply_tax(Receipt(discounted_order_value=307.5), 6.85))

class ApplyDiscount(TestCase):
    def test_no_discount(self):
        self.assertEqual(
            Receipt(
                order_value=999.99,
                discount_rate_pc=0,
                discount_amount=0,
                discounted_order_value=999.99),
            apply_discount(Receipt(order_value=999.99)))

    def test_1000USD(self):
        self.assertEqual(
            Receipt(
                order_value=1000,
                discount_rate_pc=3,
                discount_amount=30,
                discounted_order_value=970),
            apply_discount(Receipt(order_value=1000)))

    def test_4999USD(self):
        self.assertEqual(
            Receipt(
                order_value=4999.99,
                discount_rate_pc=3,
                discount_amount=150,
                discounted_order_value=4849.99),
            apply_discount(Receipt(order_value=4999.99)))

    def test_5000USD(self):
        self.assertEqual(
            Receipt(
                order_value=5000,
                discount_rate_pc=5,
                discount_amount=250,
                discounted_order_value=4750),
            apply_discount(Receipt(order_value=5000)))

    def test_6999USD(self):
        self.assertEqual(
            Receipt(
                order_value=6999.99,
                discount_rate_pc=5,
                discount_amount=350,
                discounted_order_value=6649.99),
            apply_discount(Receipt(order_value=6999.99)))

    def test_7000USD(self):
        self.assertEqual(
            Receipt(
                order_value=7000,
                discount_rate_pc=7,
                discount_amount=490,
                discounted_order_value=6510),
            apply_discount(Receipt(order_value=7000)))

    def test_9999USD(self):
        self.assertEqual(
            Receipt(
                order_value=9999.99,
                discount_rate_pc=7,
                discount_amount=700,
                discounted_order_value=9299.99),
            apply_discount(Receipt(order_value=9999.99)))

    def test_10000USD(self):
        self.assertEqual(
            Receipt(
                order_value=10000,
                discount_rate_pc=10,
                discount_amount=1000,
                discounted_order_value=9000),
            apply_discount(Receipt(order_value=10000)))

    def test_49999USD(self):
        self.assertEqual(
            Receipt(
                order_value=49999.99,
                discount_rate_pc=10,
                discount_amount=5000,
                discounted_order_value=44999.99),
            apply_discount(Receipt(order_value=49999.99)))

    def test_50000USD(self):
        self.assertEqual(
            Receipt(
                order_value=50000,
                discount_rate_pc=15,
                discount_amount=7500,
                discounted_order_value=42500),
            apply_discount(Receipt(order_value=50000)))
