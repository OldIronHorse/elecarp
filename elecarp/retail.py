#!/usr/bin/env python3
from sys import argv
from collections import namedtuple


Receipt = namedtuple(
    'Receipt',
    'quantity price order_value tax_rate_pc tax_amount total '
    'discount_rate_pc discount_amount discounted_order_value')
Receipt.__new__.__defaults__ = (None,) * len(Receipt._fields)

def order_value(r):
    return r._replace(order_value=round(r.quantity*r.price, 2))    

def apply_tax(receipt, tax_rate_pc):
    tax_amount = round(receipt.discounted_order_value*tax_rate_pc/100, 2)
    return receipt._replace(
        tax_rate_pc=tax_rate_pc,
        tax_amount=tax_amount,
        total=round(receipt.discounted_order_value+tax_amount, 2))

def apply_discount(r):
    if r.order_value >= 50000:
        discount_rate_pc = 15
    elif r.order_value >= 10000:
        discount_rate_pc = 10
    elif r.order_value >= 7000:
        discount_rate_pc = 7
    elif r.order_value >= 5000:
        discount_rate_pc = 5
    elif r.order_value >= 1000:
        discount_rate_pc = 3
    else:
        discount_rate_pc=0
    discount_amount = round(r.order_value * discount_rate_pc / 100, 2)
    return r._replace(
        discount_rate_pc=discount_rate_pc,
        discount_amount=discount_amount,
        discounted_order_value=r.order_value-discount_amount)

tax_rate_by_state = {
    'UT': 6.85,
    'NV': 8,
    'TX': 6.25,
    'AL': 4,
    'CA': 8,
    }

def calculate_receipt(args):
    tax_rate_pc = tax_rate_by_state[args[2]]
    receipt = Receipt(quantity=int(args[0]), price=float(args[1]))
    receipt = order_value(receipt)
    receipt = apply_discount(receipt)
    receipt = apply_tax(receipt, tax_rate_pc)
    return receipt

def parse_args(args):
    return int(args[0]), float(args[1]), args[2]

if __name__ == '__main__':
    try:
        quantity, price, state = parse_args(argv[1:])
        receipt = calculate_receipt(argv[1:])
        print('Order: {} @ ${:.2f} => ${:.2f}'.format(
            receipt.quantity, receipt.price, receipt.order_value))
        print('Less discount at {}% of ${:.2f} => ${:.2f}'.format(
            receipt.discount_rate_pc, receipt.discount_amount,
            receipt.discounted_order_value))
        print('Plus tax at {}% of ${:.2f} => ${:.2f}'.format(
            receipt.tax_rate_pc, receipt.tax_amount, receipt.total))
    except KeyError:
        print('Invalid state code:', state)
    except ValueError:
        print('Invalid arguments')
        print('Usage: retail.py <quantity> <price> <state>')
        print('    <quantity> n')
        print('    <price>    d')
        print('    <quantity> cc')
