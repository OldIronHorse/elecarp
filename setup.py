from setuptools import setup

setup(
  name='elecarp',
  version='0.1',
  description='Elephant Carpaccio dry run',
  url='https://gitlab.com/oldironhorse/elecarp',
  author='Simon Redding',
  author_email='s1m0n.r3dd1ng@gmail.com',
  license='GPL 3.0',
  packages=['elecarp'],
  install_requires=[
  ],
  test_suite='nose.collector',
  tests_require=['nose'],
  zip_safe=False)
